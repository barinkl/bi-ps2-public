~~NOTOC~~

====== MI-SWE.16 - Semantic web and Linked Data ======
  * vyučující: RNDr. Jakub Klímek, Ph.D.
  * email: [[klimek@fit.cvut.cz?subject=[MI-SWE]|klimek@fit.cvut.cz]], předmět musí začínat [MI-SWE]

===== MI-SWE.16 2017=====
[[https://www.ted.com/talks/tim_berners_lee_on_the_next_web?language=en|Tim Berners-Lee TED talk on Linked Data]]
[[http://doc.lmcloud.vse.cz/sparqlab/|SPARQLab - cvičebnice SPARQLu nad daty ČSSZ]]

==== Plán ====
  * 12 [[lectures:start|přednášek]]
    * Místo: TH:A-s135 (-1. patro budovy A)
    * Čas: Čtvrtek, 7:30-9:00
    * **26.10. se přednáška nekoná**
    * Místo poslední (4.1.) proběhne prezentace semestrálních prací skupiny 101
<note important>Je **silně doporučeno** chodit na přednášky, protože práce na cvičeních vyžaduje znalosti z přednášek</note>
  * 5 [[labs:start|cvičení]] skupina 101, lichý týden
    * Místo: T9:303
    * Čas: Čtvrtek 9:15-10:45 (liché)
    * Data konání výčtem: 12.10., **26.10. NE**, 9.11., 23.11., 7.12., 21.12., **4.1. prezentace semestrálních prací skupiny 102**
  * 5 [[labs:start|cvičení]] skupina 102, sudý týden
    * Místo: T9:303
    * Čas: Čtvrtek 9:15-10:45 (sudé)
    * Data konání výčtem: 5.10., **19.10. NE**, 2.11., 16.11., 30.11., 14.12.

==== Hodnocení ====
  * Zápočet za semestrální úlohu
    * Maximum 30 bodů
    * Minimum 15 na zápočet, nutná podmínka ke zkoušce
    * 5 částí, 2 fáze odevzdávání
      * 1. fáze obsahuje část 1 a 2 (Vybrané datasety převedené do RDF) až za 10 bodů
      * 2. fáze obsahuje části 3, 4, 5 (Linkování, Dotazy, Aplikace a Prezentace) až za 20 bodů
      * Obě fáze se odevzdávají stejným způsobem, a to nahráním zip souboru do EDUXu tak, aby byl přístupný ke stažení na adrese https://edux.fit.cvut.cz/courses/MI-SWE.16/_media/student/<yourlogin>/semestral.zip kde <yourlogin> je váš login
<note>DEADLINE pro odevzdání 1. fáze byla **22.11.2017 20:00** pro skupinu 101</note>
<note>DEADLINE pro odevzdání 1. fáze byla **15.11.2017 20:00** pro skupinu 102</note>
<note>DEADLINE pro odevzdání 2. fáze byla **3.1.2018 20:00** pro všechny</note>
<note warning>Na úlohy odevzdané po DEADLINE nebude brán zřetel, nepište žádosti o odklad</note>
  * Písemná zkouška
    * až 70 bodů
  * Celkem až 100 bodů dle standardní stupnice

/**===== MIE-SWE.16 =====
*==== Plan ====
*  * 10 [[lectures:start|lectures]]
*    * Place: TH:A-942
*    * Time: Wednesday, 9:15-10:45, **except 16.11. and 14.12.**
*    * Instead of the last one (4.1.) you will present your semestral work
*<note important>It is **highly recommended** to attend the lecture, the work in seminars requires knowledge gained in lectures </note>
*  * 6 [[labs:start|labs]], odd week
*    * Place: T9:350
*    * Time: Tuesday 7:30-9:00 (odd)
*    * List of dates of seminars: 12.10., 26.10., 9.11., 23.11., 7.12., 21.12.
*
*==== Evaluation ====
*  * Assessment for semestral work
*    * Maximum 30 points
*    * Minimum 15 to get assessment, needed to continue to written exam
*    * 6 parts, 2 phases of turning in
*      * First phase contains parts 1 and 2 (Chosen datasets transformed to RDF) for up to 10 points
*      * Second phase contains parts 3, 4, 5 and 6 (Linking, Querying, Application, Presentation) for up to 20 points
*      * Both phases are turned in by the same means, i.e. by uploading a zip file to EDUXu so that it is accessible for download at https://edux.fit.cvut.cz/courses/MI-SWE.16/_media/student/<yourlogin>/semestral.zip where <yourlogin> is your login
*<note >DEADLINE for the first phase was **8.11.2016 20:00**</note>
*<note >DEADLINE for the second phase was **3.1.2017 20:00**</note>
*<note warning>Work turned in after DEADLINE will be ignored, do not ask for possibilities to turn in later</note>
*  * Written exam
*  * up to 70 points
*  * Total of up to 100 points - standard scale
*/