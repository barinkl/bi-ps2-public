~~NOTOC~~

====== BI-FIP - Účetnictví a finance podniku ======
  
* vyučující: [[:teacher:start]]

===== Aktuality =====

{{blog>:news?10}}

===== Přístup k materiálům =====

<note important>
Veškeré studijní materiály i agenda jsou umístěny v **[[http://moodle.cvut.cz|systému Moodle ČVUT]]** , předmět BI-FMU - Finanční a manažerské účetnictví.
</note>    
