====== MI-AAK - Aritmetika a kódy ======

<note important>Předmět MI-AAK.1 se již nevyučuje.</note>

/*

<note important>Předmět MI-AAK je nahrazen předmětem[[https://edux.fit.cvut.cz/courses/MI-AAK.1/start|MI-AAK.1]]</note>


V předmětu jsou prezentovány možnosti opravy a zjišťování chyb a jejich shluků v datech ukládaných do pamětí nebo přenášených různými kanály.
Dále jsou prezentovány algoritmy základních i složitějších aritmetických operací, zejména ty, které jsou vhodné pro implementaci logickými obvody.

===== Osnovy přednášek a cvičení a harmonogram =====

^týden^datum^přednáška      ^cvičení  |
|  1.  |  st 18.2.2015  | Bezpečnostní kódy - základní principy.   | Konečná  tělesa.  |
|  2.  |  st 25.2.2015  | Lineární kódy.   | Mnohočleny nad konečnými tělesy.  |
|  3.  |  st 4.3.2015  | Cyklické kódy.   | Lineární kódy a opravy chyb.  |
|  4.  |  st 11.3.2015  | Opravy shluků chyb.   | Cyklické kódy.  |
|  5.  |  st 18.3.2015  | Opravy několika chyb.   | Opravy shluků chyb.  |
|  6.  |  st 25.3.2015  | Číselné soustavy, sčítání a odčítání.   | Opravy několika chyb.  |
|  7.  |  st 1.4.2015  | Kódy pro zobrazení čísel se znaménkem.   | TEST.  |
|  8.  |  st 8.4.2015  | Násobení.   | Číselné soustavy a kódy.  |
|  9.  |  st 15.4.2015  | Pohyblivá řádová čárka.   | Sčítačky a odčítačky.  |
|  10.  |  st 22.4.2015  | Dělení.   | Násobení a násobičky.  |
|  11.  |  st 29.4.2015  | Elementární funkce.   | Pohyblivá řádová čárka.  |
|  12.  |  st 6.5.2015  | Rezerva.   | Zápočet.  |
|  13.  |  st 13.5.2015  |  rektorský den   | |
===== Doporučená literatura =====

Pluháček A.: Projektování logiky počítačů. Praha, ČVUT 2000. ISBN 80-01-02145-9.
 \\
Hlavička J. - Racek S. - Golan P. - Blažek T.: Číslicové systémy odolné proti poruchám. Praha, ČVUT 1992. ISBN 80-01-00852-5. 
 \\
Adámek J.: Kódování. Praha, SNTL 1989. 
 \\
Parhami B.: Computer Arithmetic: Algorithms and Hardware Designs. Oxford University Press 1999. ISBN 0195125835. 
 \\
Koren I.: Computer Arithmetic Algorithms (2nd edition). A K Peters 2001. ISBN 1568811608. 
 \\
Muller J. M.: Elementary Functions: Algorithms and Implementation (2nd edition). Boston, Birkhäuser 2005. ISBN 0817643729. 
 \\
Moreira J. C. - Farrell P. G.: Essentials of Error-Control Coding. Wiley 2006. ISBN 047002920X. 
 \\
Lin S. - 
*/