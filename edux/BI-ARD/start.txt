~~NOTOC~~

====== BI-ARD - Interaktivní aplikace s Arduinem ======

//**Oficiální stránka předmětu**//

  * vyučující: [[:teacher:start]]


===== Aktuality =====

{{blog>:news?10}}

===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**
   

^ Cvičení ^ Datum (Po) ^ Datum (Út) ^ Téma ^ Body ^
| 1. | 19. 2. | 20. 2. | [[tutorials:01:start|]] | 5 |
| 2. | 26. 2. | 27. 2. | [[tutorials:02:start|]] | 5 |
| 3. | 5. 3. | 6. 3. | [[tutorials:03:start|]] | 5 |
| 4. | 12. 3. | 13. 3. | [[tutorials:04:start|]] | 7 |
| 5. | 19. 3. | 20. 3. | [[tutorials:05:start|]] | 10 |
| 6. | 26. 3. | 27. 3. | [[tutorials:06:start|]] | 10+3 |
| 7. | 9. 4. | 3. 4. | [[classification:semestralka|7. Semestrální práce]], [[classification:co_nedelat|co nedělat s Arduinem]] | 0 |
| 8. | 16. 4. | 10. 4. | [[tutorials:08:start|]], [[classification:semestralka|Semestrální práce]] | 0 |
| 9. | 23. 4. | 17. 4. | [[tutorials:10:start|9. Semestrální práce, dokumentace a čitelnost kódu]] | 0 |
| 10. | 30. 4. | 24. 4. | [[tutorials:09:start|10. Demo semestrální práce]] | 15 |
| 11. | 7. 5. | **10. 5. (Čt)** | [[classification:bonus|11. Semestrální práce, bonusová úloha]] | 0+10 |
| 12. | 14. 5. | 15. 5. | [[tutorials:12:start|]] | 40 |