~~NOTOC~~

====== MI-LIF - Podnikové portály ======

//**Oficiální stránka předmětu**//

  * vyučující: [[:teacher:start]]

<note important>  Předmět MI-LIF proběhne stejně jako vloni a předloni formou **intenzivního kurzu, pravděpodobně v termínu 5.-7.1. 2015**. V průběhu zkouškového pak budete zpracovávat projekty, které budou započítány do zkoušky. K tomu bude přidán ještě zkouškový test (stejně jako vloni). </note>

<note>Jakmile bude úplně jasno, oslovíme zapsané studenty mailem.</note>

===== Aktuality =====
  
{{blog>:news?10}}

===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**
   
