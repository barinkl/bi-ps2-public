~~NOTOC~~
====== BI-BEZ - Bezpečnost ======


===== Ukázka příkladů pro 1. test =====
Ukázka některých možných příkladů: {{:test1-priklady.pdf|}}

===== Ukázka příkladů pro 2. test =====
Ukázka některých možných příkladů: {{:test2_priklady.pdf|}}

/*
neplati:
===== Náhradní a opravné testy =====
Jak bylo avizováno na proseminářích, je vypsán termín náhradního a opravného testu:
  * **Náhradní** test si mohou napsat studenti, kteří nepsali test v řádném termínu kvůli nemoci nebo z jiných závažných důvodů a jsou řádně omluveni. (Omluvenku s sebou.)
  * **Opravný** test si mohou napsat studenti, kteří si potřebují vylepšit bodové hodnocení a mají z laboratorních cvičení celkem **minimálně <del>10 bodů</del> 8 bodů za prvních 5 úloh**.
    * Bodové hodnocení **opravného** testu se **krátí o 20%** ze získaného bodového hodnocení.
    * Účastí na opravném testu se ruší (nuluje) bodové hodnocení testu psaného v řádném termínu.
  * Na termín náhradního a opravného testu se hlašte do jednorázové akce v informačním systému (7. 5. 2015).

*/

===== Program přednášek =====
^ Číslo ^ Téma přednášky ^
|  1.| {{:bez_n1.pdf|Základní pojmy v kryptologii, substituční šifry. Blokové, transpoziční šifry}} |
|  2.| {{:bez_n2.pdf|Exponenciální šifra, zřízení společného klíče a problém diskrétního logaritmu}} | 
|  3.| {{:bez_n3.pdf|Rozdělení šifer, proudové šifry, RC4, A5/1}} |
|  4.| {{:bez_n4.pdf|Blokové šifry, DES, 3DES, AES, operační módy blokových šifer}} |
|  5.| {{:bez_5.pdf|Hašovací funkce, MD5, SHA-x, HMAC}}.\\ Odkazované obrázky: {{:md5_b1.pdf|MD5 schéma zpracování zprávy}}, {{:md5_b2.pdf|MD5 kompresní funkce}}, {{:tabulka_ki.pdf|MD5 tabulka Ki}}; {{:sha_b1.pdf|SHA-1 schéma zpracování zprávy}}, {{:sha_b2.pdf|SHA-1 kompresní funkce}}. {{:bez5.zip|Celá přednáška jako ZIP}}.|
|  6.| {{:bez_n6.pdf|RSA, problém faktorizace. Kryptografie s veřejným klíčem, El-Gamalův algoritmus, DSA}} |
|  7.| {{:bez_n7.pdf|Bezpečnost kryptografických systémů z hlediska teorií informace a složitosti}} |
|  8.| {{:bez_n8.pdf|Základy kryptografie eliptických křivek a kvantové kryptografie}} |
|  9.| {{:bez_n9.pdf|Generování klíčů, Rabin-Millerův test, náhodné generátory}} |
|  10.| {{:bez_n11.pdf|Úvod do zabezpečení pomocí čipových karet}} |
|  11.| {{:bez_n10.pdf|Infrastruktura veřejného klíče}} |
|  12.| {{:bez_n12.pdf|IT bezpečnost}} |

===== Program cvičení =====
Program je předběžný, bude postupně upřesněn.

^ Číslo ^ Téma laboratorního cvičení ^
|  1.| [[seminars:01:start|Opakování aritmetiky modulo m – sčítání, odčítání, násobení, inverze, umocňování, gcd, lcm.]] |
|  2.| [[labs:01:start|Substituční a transpoziční šifra, afinní šifry.]] |
|  3.| [[seminars:02:start|Hillova šifra, modulární umocňování, blokové šifry]]. |
|  4.| [[seminars:02:start|Diffie-Hellman algoritmus a problém diskrétního logaritmu]]. |
|  5.| **TEST 1.** [[labs:02:start|OpenSSL: Seznámení s knihovnou, hašovací funkce, proudové šifry]]. |
|  6.| [[labs:03:start|OpenSSL: Blokové šifry a operační módy]]. |
|  7.| [[seminars:03:start|Narozeninový paradox, vzdálenost jednoznačnosti]]. |
|  8.| [[labs:04:start|OpenSSL: Asymetrická kryptografie – generování klíčů, elementární operace RSA]]. |
|  9.| **TEST 2.** [[seminars:04:start]].|
|  10.| [[labs:05:start|OpenSSL: Použití certifikátů v programu]] |
|  11.| [[labs:06:start|OpenSSL: Ověřování certifikátů, dohoda klienta a serveru na použitých kryptografických algoritmech]]. |
|  12.| Náhradní termín testu, zápočet |

V laboratořích bude používán software
  * Mathematica - pro spouštění na vlastním počítači použijte [[https://edux.fit.cvut.cz/courses/BI-CAO/manual/tunnel|Tento návod]]
  * Překladač jazyka C (GCC), přípd. vhodné IDE
  * OpenSSL - příkazový řádek + knihovna pro vývoj programů v jazyce C/C++

===== Poznámky =====
  * Pro přístup k materiálům je vyžadováno přihlášení.
   