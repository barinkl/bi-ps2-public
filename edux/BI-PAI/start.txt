~~NOTOC~~

====== BI-PAI - Právo a informatika ======



Předmět Právo a informatika pro české studenty v denním i kombinovaném studium v akademickém roce 2017/2018 přednáší i zkouší **JUDr. Zdeněk Kučera, Ph.D.**. Hostujícími přednášejícími jsou **JUDr. Matěj Myška, Ph.D.** a **JUDr. Ing. Michal Matějka**.


The subject Law and Informatics in the English language in academic year 2017/2018 lectures **JUDr. Zdeněk Kučera, Ph.D.**. The hosting lecturers are **JUDr. Matěj Myška, Ph.D.**, **JUDr. Ing. Michal Matějka** and **Mgr. Alžběta Krausová, LL.M**.


//Zdeněk Kučera, garant předmětu//


**AKTUALITY**
----------------------------------------------

Přeji studentům úspěšný start do nového akademického roku 2017/2018.

//Zdeněk Kučera//

