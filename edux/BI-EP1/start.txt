~~NOTOC~~

====== BI-EP1 - Efektivní programování 1 ======

//**Oficiální stránka předmětu**//

BI-EP1 je zamýšleno jako výběrový předmět pro nadané studenty. Upozorňujeme na to, že náročnost bude vyšší, než u průměrného předmětu se stejnou hodinovou dotací...

V rámci předmětu jsou řešeny vybrané algoritmické úlohy. Hodnocení úloh je automatické, řešení by mělo být správné pro jakýkoli vstup.
Po skončení každého cyklu jsou diskutovány různé možnosti řešení, jejich výhody a nevýhody.

  * [[material:]] z přednášek a cvičení
  * [[schedule:]] po jednotlivých týdnech
  * [[classification:]] předmětu; aktuální stav vašich bodů najdete v záložce Studijní výsledky
  * vyučující: [[:teacher:start]]
  * vyhodnocovací systém: [[http://judge.fit.cvut.cz:8080/ep/]]
  * stránky soutěže v programování CTU Open: [[http://contest.felk.cvut.cz/17prg/]]

===== Vyhodnocovací systém =====

V předmětu BI-EP1 se používá vyhodnocovací systém PCSS běžící na adrese [[http://judge.fit.cvut.cz:8080/ep/]].
Pro odevzdávání je možné použít následující programovací jazyky:

^Jazyk    ^ Přípona    ^ Překladač         ^ Podstatné přepínače  ^
|**C**    | ''.c''     | GCC verze 4.9.3   | ''-std=c11 -O2 -Wall -lm''  |
|**C++**  | ''.C''     | GCC verze 4.9.3   | ''-std=c++14 -O2 -Wall -lm -lstdc++''  |
|**Java** | ''.java''  | Sun JDK verze 1.8.0  | N/A  |


===== Aktuality =====

{{blog>news?3}}

===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**
   
