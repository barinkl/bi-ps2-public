~~NOTOC~~

====== BI-PSI - Počítačové sítě ======

//**Oficiální stránka předmětu**//

  * vyučující: [[:teacher:start]]

===== Přednášky =====


^  číslo   ^  přednáška  ^ 
|   1| Úvod. Historie počítačových sítí. Modely OSI a TCP/IP.  |
|   2| Fyzická vrstva, kódování, multiplex, média.   | 
|   3| Linková vrstva. Metody přístupu ke sdílenému mediu.  |
|   4| Síťová vrstva,  síťové adresy, IPv4.  | 
|   5| Síťová vrstva II, IPv6, ATM, MPLS.  | 
|   6| Propojování sítí. Směrovací protokoly. |
|   7| Transportní vrstva. TCP, UDP. | 
|   8| Aplikační vrstva: služby a protokoly.  | 
|   9| Řízení toku. QoS. |
|  10| Optické sítě.  | 
|  11| Adresářové služby, DNS. | 
|  12| Správa sítí. | 

/*
^  týden  ^  přednáška  ^
|  1      | 1. Úvod. Historie počítačových sítí. Modely OSI a TCP/IP.  |
|  2      | 2. Fyzická vrstva: kódování, multiplex, média - vlastnosti, použití, zapojení.  |
|  3      | 3. Linková vrstva: potvrzování, protokoly. Metody přístupu ke sdílenému médiu.  | 
|  4      | 4. Síťová vrstva,  síťové adresy. Směrování. |
|  5      | 5. Propojování sítí. Směrovací protokoly. MPLS.  |
|  6      | 6. Transportní vrstva. TCP, UDP.  |
|  7      | 7. Aplikační vrstva: služby a protokoly. IPv6. |
|  8      | 8. Optické sítě. ATM. |
|  9      | 9. Řízení toku. QoS.  |
|  10     | 10. Adresářové služby: DNS, X.500.  |
|  11     | 11. Bezpečnost: šifrování, certifikáty, digitální podpis.  |
|  12     | 12. Zabezpečení sítí a nástroje správy. |
*/

===== Prosemináře =====

^  týden  ^  téma prosemináře  ^
|  1  | Úvod do lokálních sítí, základní pojmy  |
|  2  | Programové rozhraní BSD Socketů. |
|  3  | Překlad adres, IPv6  |
|  4  | Komunikační schémata, efektivita linkových protokolů.  |
|  5  | Směrovací protokoly, příprava na test |
|  6  | praktické ukázky konfigurace síťových prvků |

===== Laboratorní cvičení =====

^  týden  ^  cvičení  ^
|  1  | Adresace, konfigurace síťového rozhraní, práce s aplikací Wireshark   |
|  2  | Konfigurace segmentu lokální sítě, DHCP  |
|  3  | **TEST č.1**, Konfigurace segmentu lokální sítě s IPv6 |
|  4  | Připojení segmentu lokální sítě k internetu, NAT |
|  5  | Komunikace mezi segmenty lokální sítě, směrování |
|  6  | **TEST č.2**, **ODEVZDÁNÍ TCP - poslední termín**  |



===== Aktuality =====


===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**
   
