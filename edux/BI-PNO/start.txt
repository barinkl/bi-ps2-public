~~NOTOC~~

====== BI-PNO - Praktika v návrhu číslicových obvodů ======

[[http://www.ulozto.cz/xrsUTNd/bi-pno-tutorial-zip|Video tutoriál instalace ISE+ModelSim]]


^  **#**  ^   Datum   ^  Přednáška   ^   Cvičení   ^ 
| 1 |   3.10. | [[lectures:01:start|]]  | 1-bitová sčítačka: založení projektu, popis obvodu, syntéza, nahrání do přípravku, simulace  | 
| 2 |  10.10. | [[lectures:02:start|]]  | [[labs:02:01:start]]\\      [[labs:02:02:start]]  | 
| 3 |  17.10. | [[lectures:03:start|]]  | [[labs:03:start]]    - [[labs:03:solution|Řešení]]   |
| 4 |  24.10. | [[lectures:04:start|]]  | [[labs:04:start]]    - [[labs:04:solution|Řešení]]   | 
| 5 |  31.10. | [[lectures:05:start|]]  | [[labs:05:start|Projekt 1]]  | 
| 6 |   7.11. | [[lectures:06:start|]]  | [[labs:05:start|Projekt 1]], odevzdání (10 bodů)  | 
| 7 |  14.11. | [[lectures:07:start|]]  | [[labs:07:start|Projekt 2]] - testbench (T)  - //přednáška v SAGE: Miloš Bečvář: Low Power Design//  | 
| 8 |  21.11. | [[lectures:08:start|]]  | [[labs:07:start|Projekt 2]] - testbench (T), odevzdání T (10 bodů)  | 
| 9 |  28.11. | **[[lectures:09:start|]]**  | [[labs:07:start|Projekt 2]] - návrh (N)  |
| 10 |   5.12. | [[lectures:10:start|]]  | [[labs:07:start|Projekt 2]] - ladění (L), odevzdání N+L (10 bodů) |
| 11 |  12.12. | [[lectures:11:start|]]  | [[labs:07:start|Projekt 2]] - integrace (I)  | 
| 12 |  19.12. | [[lectures:12:start|]]  | [[labs:07:start|Projekt 2]] - integrace (I), odevzdání I (10 bodů), ZÁPOČET   |
| 13 |         | [[lectures:13:start|]]  |   |


/*
  * vyučující: [[:teacher:start]]

===== Aktuality =====

{{blog>:news?10}}

===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**
   
*/