~~NOTOC~~

====== BI-SRC - B ======

//**Oficiální stránka předmětu**//

  * vyučující: [[:teacher:start]]



===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**

====== Přednášky a laboratoře: aktuální rozvrh pro školní rok 2016/17 ======


^ týden   ^  datum přednášky-cvičení     ^ téma  ^ cvičení - laboratoř  ^ materiály ^
| 1 |  | [[lectures:01:start|]] Úvod  |  [[tutorials:01:start|]]   |   |
| 2 |   | [[lectures:03:start|]]  |  [[tutorials:02:start|]]  | |
| 3 |   | [[lectures:04:start|]]  |  [[tutorials:03:start|]]  |  |
| 4 |   | [[lectures:02:start|]]  |  [[tutorials:04:start|]]  |    |
| 5 |   | [[lectures:02:start|]] dokončení, příklady |  [[tutorials:05:start|]]  |    |
| 6 |   | [[lectures:06:start|]]   |  [[tutorials:06:start|]]   |   |
| 7 |   | [[lectures:07:start|]]  |  [[tutorials:08:start|]]  |   |
| 8 |   | [[lectures:08:start|]]  |  Test 1   | obsah testu: [[classification:start|]]  |
| 9 |  | [[lectures:09:start|]]  |  [[tutorials:09:start|]]  |   |
| 10 |  | [[lectures:10:start|]]   |  [[tutorials:10:start|]]   |   |
| 11 |  | [[lectures:12:start|]]  |  [[tutorials:13:start|]]  |  |
| 12 |  | [[lectures:13:start|]], Test 2  |  řešení úloh, zápočet  | obsah testu: [[classification:start|]]  |
   
