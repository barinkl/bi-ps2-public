====== BI-PRP - Právo a podnikání ======

//**Oficiální stránka předmětu**//

Předmět Právo a podnikání v akademickém roce 2016/2017 přednáší i zkouší **JUDr. Zdeněk Kučera, Ph.D.**

Hostujícími přednášejícími jsou **JUDr. Ing. Libor Kyncl, Ph.D.** a **JUDr. Ing. Michal Matějka**.

__Předběžný program přednášek:__

28.2. Správa obchodní korporace, formy spolupráce na projektech, financování projektů, exit z projektů (Kučera)
{{:prp_1_2017.pdf|}}

7.3. Smluvní právo se zaměřením na SW (Kučera)

{{:prp_2_2017.pdf|}}

14.3. IT smlouvy v praxi (Kučera)
{{:3_-_prp_2_2017-1-1.pdf|}}

21.3. Úvod do daňového práva (Kyncl)
{{:4_-_pap_cz_4_2017.ppt|}}

28.3. Marketing na internetu (Kučera)
{{:5_-_prp2017-5fin-1.pdf|}}

4.4. Ochrana dat na internetu (Kučera)
{{:ochrana_dat17.pdf|}}


11.4. Práva k duševnímu vlastnictví v podnikatelské praxi, přihlášení ochranné známky (Matějka)
{{:sprava_dusevniho_vlastnictvi_2017.pdf|}}

18.4. Trestní právo v podnikatelské praxi (Matějka)
{{:uvod_do_trestniho_prava_2017.pdf|}} 

25.4. Spory v podnikání (Kučera) 
{{:prp_8-2016.pdf|}}

9.5. Právo na sociálních sítích (Kučera)
{{:prp_soc_site.pdf|}}

16.5. Případové studie (Kučera)

**změna vyhrazena**

__ZKOUŠKA__

Výlučně formou písemného testu

20 otázek (a-d), jedna odpověď správná, nebo je odpovědí 1 slovo/číslo/sousloví - za správnou 1 bod

15 otázek/úloh s otevřenou odpovědí, za zcela správnou 2 body, za částečně správnou 1 bod

celkem tedy možnost získání 50 bodů

45 minut na vypracování

__Hodnocení testu__
A: 45-50 bodů
B: 40-44 bodů
C: 35-39 bodů
D: 30-34 bodů
E: 25-29 bodů
F: méně než 25 bodů

Povinné studijní materiály jsou přednášky (jak ppt prezentace, tak látka odpřednášená během ústních přednášek), příp. literatura, která je uvedena v rámci přednášek jako povinná.

Veškeré zákony v aktuálním znění naleznete na [[http://portal.gov.cz]]

Klasifikace se řídí [[http://www.cvut.cz/pracoviste/pravni-odbor/dokumenty/studijni-predpisy/studijnirad.pdf|Studijní a zkušební řád ČVUT]].

Přeji všem hodně štěstí u zkoušky!

===== Aktuality =====

Výsledky z testu 26.6.
{{:test2606.pdf|}}


Výsledky z testu 5.6.

{{:vysledky5-6.pdf|}}

Výsledky z testu 16.5.

{{:prp2017i.pdf|}}

Výsledky z testu 23.5.

{{:test2.pdf|}}