~~NOTOC~~

====== BI-BLE - Blender ======

//**Oficiální stránka předmětu**//

Předmět volně navazuje na představení opensource systému Blender v předmětu BI-MGA (Multimediální a grafické aplikace). Je určený zájemcům o 3D grafiku a animace. Nabízí kompletní a prakticky zaměřené seznámení s tímto prostředím. Studenti mohou dále pokračovat předmětem BI-PGA (Programování grafických aplikací). 

  * vyučující: [[:teacher:start]]

===== Aktuality =====

{{blog>:news?10}}

===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**
   
