~~NOTOC~~

====== BI-ST3 - Síťové technologie 3 ======

//**Oficiální stránka předmětu**//

Předmět je zaměřen na získání základních znalosti z oblasti počítačových sítí a praktických zkušeností se síťovými technologiemi. Předmět odpovída látce kurikula Cisco Netacad programu - CCNA3 - R&S Scaling networks.

Předmět BI-ST3 je navazujícím kurzem na předměty BI-ST1 a BI-ST2. Principy routování a přepínání budou v tomto kurzu dále prohloubeny a rozšířeny. Studenti budou schopni vyladit nastavení protokolů a získat další výhody jako např. zvýšená účinnost, predikovatelnost, rozšíření nad rámec běžné topologie, bezpečnosti, atd.

Studenti předmětu se naučí, jakým způsobem vytvářet redundantní, predikovatelné a efektivní síťové topologie. Redundance je dosahována agregací switchportů do virtuálních kanálů (Etherchannel). Na 3. vrstvě OSI modelu je redundance dosahována agregací více směrovačů jako virtuálních (fantomových) směrovačů (VRRP, HSRP a GLBP protokoly). Studenti se naučí, jak vyvažovat síťový provoz a dosahovat vyšší efektivity. Toho je dosahováno pomocí per-VLAN STP (PVSTP) na druhé vrstvě OSI modelu a směrováním per-VLAN na 3. vrstvě anebo mnoha paralelních cest ve směrovací tabulce EIGRP nebo OSPF. Studenti se také naučí jak rozšiřovat síťové topologie velikosti kampusu nebo  dokonce mnoha-kampusů s pomocí tzv. multiple-area OSPF směrování. Dalším cílem je porozumění rolím tunelů (šifrovaných anebo prostých) a jejich integraci se sítěmi podporujícími protokoly IPv4 a IPv6 směrování v duálním síťovém stacku.