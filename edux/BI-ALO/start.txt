====== Algebra a logika (BI-ALO 2018) ======

==== literatura ====

Výchozím textem jsou vybrané kapitoly z [[http://users.fit.cvut.cz/~staryja2/BIMLO/matematicka-logika.pdf|Úvodu do matematické logiky]]
a původní články na jednotlivá témata - postupně se zde budou objevovat.

  * L. Henkin: [[http://users.fit.cvut.cz/~staryja2/BIALO/henkin-the-completeness-of-the-first-order-calculus.pdf|The completeness of the first order calculus]] (úplnost logiky)
  * L. Henkin: [[http://users.fit.cvut.cz/~staryja2/BIALO/henkin-the-discovery-of-completeness-proofs.pdf|The discovery of my completeness proofs]] (osobní vzpomínky a historický kontext)
  * J. A. Robinson: [[http://users.fit.cvut.cz/~staryja2/BIALO/robinson-a-machine-oriented-logic-based-on-the-resolution-principle.pdf|A Machine-Oriented Logic Based on the Resolution Principle]] (rezoluce v predikátové logice, unifikační algoritmus)
  * G. Cantor: [[http://users.fit.cvut.cz/~staryja2/BIALO/cantor-transfinite-numbers.djvu|Transfinite Numbers]] (počátky teorie množin, ordinální a kardinální čísla).
  * B. Balcar, P. Štěpánek: Teorie množin
  * T. Jech: Set Theory

==== 2017 ====

  * **20.02.** Úplnost predikátové logiky.
  * **27.02.** Konzervativní rozšíření, Henkinovské zúplnění.
  * 27.02. Teorie grup, okruhů, těles. Důsledky kompaktnosti.
  * **06.03.** Prenexní tvar formulí.
  * 06.03. Teorie grup, okruhů, těles. Důsledky kompaktnosti.
  * **13.03.** Konzervativní rozšiřování teorie o predikáty a funkce.
  * 13.03. Prenex, Skolemizace.
  * **20.03.** Rezoluční metoda: Skolemizace, Herbrandovské universum, saturace.
  * 20.03. Prenex, Skolemizace.
  * **27.03.** Rezoluční metoda: unifikace, rezoluce.
  * 27.03 Unifikace a rezoluce
  * **03.04.** Věta o rezoluci. Limity rezoluční metody.
  * 03.04 Unifikace a rezoluce
  * **10.04.** Základy axiomatické teorie množin.
  * **17.04.** (Velikonoce)
  * **24.04.** Dobrá uspořádání, ordinální čísla.
  * 24.04. Vlastnosti dobrých uspořádání. [[http://users.fit.cvut.cz/~staryja2/BIALO/goodstein.ps|Goodsteinova věta]].
  * **02.05.** Konstrukce základních struktur.
  * **11.05.** Reálná čísla. Mohutnosti, kardinální čísla.
  * **15.05.** Kardinální aritmetika. Axiom výběru.
  * 15.05. Počítání mohutností, ekvivalenty axiomu výběru.


