~~NOTOC~~

====== MI-PAR.2 - Paralelní algoritmy a systémy ======

//**Oficiální stránka předmětu**//


===== Aktuality =====


==== Zkouškové termíny ====

  - řádný zkouškový termín: Po 16.1.2017 9.00-11.00 v poslucharně 155.
  - zkouškový termín bude St 25.1.2017 7.30-9.30 v poslucharně 107.
  - zkouškový termín bude St 1.2.2017 13.15-15.15 v poslucharně 155.
  - zkouškový termín bude Čt 22.6.2017 13.15-15.15 v poslucharně 105
  - zkouškový termín bude Čt 29.6.2017 14.15-16.15 v poslucharně 105

==== O předmětech MI-PAR.2 a  MI-PPR.2 ====

Tato dvojice předmětů vznikla v r. 2013 rozdělením předmětu MI-PAR.1 na dva předměty. MI-PPR.2 je ukončen zápočtem. Studentům, kteri opakují předmět MI-PAR.1 a získali zápočet z MI-PAR.1, bude tento uznán v MI-PPR.2 a nemusí si tudíž MI-PPR.2 zapisovat.

V předmětu MI-PAR.2 jsou každý týden 2 hodiny přednášek a 2 hodiny proseminářů, kde řeším na tabuli úlohy z odpřednášené látky. 100bodové hodnocení v MI-PAR.2 je řešeno rozdělením zkoušky na 2 midtermy během semestru po 30 bodech a na písemnou  zkoušku během zkouškového za 40 bodů. Studenti tedy mohou během semestru získat dostatečný počet bodů, jednak vyzkoušet své znalosti a lépe se připravit na hlavní písemnou zkoušku ve zkouškovém období.

===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**
   
