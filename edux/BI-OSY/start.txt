~~NOTOC~~

====== BI-OSY - Operační systémy ======

//**Oficiální stránka předmětu**//

  * vyučující: [[:teacher:start]]

  * rozvrh přednášek a cvičení: 
https://timetable.fit.cvut.cz/public/cz/predmety/11/21/p1121406.html


===== Aktuality =====

Pokud se nezobrazují žádné aktuality a myslíte si, že by měly, klikněte [[/courses/BI-OSY/start?purge|zde]].

{{blog>:news?10}}

===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**
   
