~~NOTOC~~

====== MI-SOC.16 - Systémy na čipu ======

//**Oficiální stránka předmětu**//

  * vyučující: [[:teacher:start]]

===== Aktuality =====

Veškeré materiály jsou na 
https://edux.fit.cvut.cz/courses/MI-SOC/

(Obsah předmětu se v nove akreditaci obsahově nezměnil).

{{blog>:news?10}}

===== Přístup k materiálům =====

  * **Pro přístup k materiálům je vyžadováno přihlášení!**
   
