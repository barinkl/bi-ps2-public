~~NOTOC~~

====== MI-AAK.1 - Aritmetika a kódy ======

<note important>Předmět MI-AAK.1 se již nevyučuje.</note>

/*

V předmětu jsou prezentovány možnosti opravy a zjišťování chyb a jejich shluků v datech ukládaných do pamětí nebo přenášených kanály.
Dále jsou prezentovány algoritmy základních i složitějších aritmetických operací, zejména ty, které jsou vhodné pro implementaci logickými obvody.

===== Osnovy přednášek a cvičení a harmonogram =====

|  1.|  st 24.2.2016  |Bezpečnostní kódy - základní principy.     |Nekonvenční aplikace číselných soustav.  |
|  2.|  st 2.3.2016  |Lineární kódy.     |Konečná  tělesa.  |
|  3.|  st 9.3.2016  |Cyklické kódy.     |Mnohočleny nad konečnými tělesy.  |
|  4.|  st 16.3.2016  |Opravy shluků chyb.     |Lineární kódy a opravy chyb.  |
|  5.|  st 23.3.2016  |Opravy několika chyb.     |Cyklické kódy a opravy shluků chyb.  |
|  6.|  st 30.3.2016  |Číselné soustavy, sčítání a odčítání.     |Opravy několika chyb.  |
|  7.|  st 6.4.2016  |Kódy pro zobrazení čísel se znaménkem.     |TEST.  |
|  8.|  st 13.4.2016  |Násobení.     |Číselné soustavy a kódy.  |
|  9.|  st 20.4.2016  |Pohyblivá řádová čárka.     |Sčítačky a odčítačky.  |
|  10.|  st 27.4.2016  |Dělení.     |Násobení a násobičky.  |
|  11.|  st 4.5.2016  |Elementární funkce.     |Pohyblivá řádová čárka.  |
|  12.|  st 11.5.2016  |  rektorský den  ||
|  13.|  st 18.5.2016  |Rezerva.     |Zápočet.  |
\\

===== Doporučená literatura =====

Pluháček A.: Projektování logiky počítačů. Praha, ČVUT 2000. ISBN 80-01-02145-9.
 \\
Hlavička J. - Racek S. - Golan P. - Blažek T.: Číslicové systémy odolné proti poruchám. Praha, ČVUT 1992. ISBN 80-01-00852-5. 
 \\
Adámek J.: Kódování. Praha, SNTL 1989. 
 \\
Parhami B.: Computer Arithmetic: Algorithms and Hardware Designs. Oxford University Press 1999. ISBN 0195125835. 
 \\
Koren I.: Computer Arithmetic Algorithms (2nd edition). A K Peters 2001. ISBN 1568811608. 
 \\
Muller J. M.: Elementary Functions: Algorithms and Implementation (2nd edition). Boston, Birkhäuser 2005. ISBN 0817643729. 
 \\
Moreira J. C. - Farrell P. G.: Essentials of Error-Control Coding. Wiley 2006. ISBN 047002920X. 
 \\
Lin S. - Costello D. J.: Error Control Coding (2nd Edition). Prentice Hall 2004. ISBN 0130426725. 

*/